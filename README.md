# ziggo-kicker

Silly script that reboots a Docsis Technicolor TC7210 modem when it crashes but the webserver stays active. I run it as a cron job every 30 minutes and has avoided getting locked out of my own server several times.
