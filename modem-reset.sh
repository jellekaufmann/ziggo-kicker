#!/bin/bash

# IP adres van de default gateway bepalen
IP=$(/sbin/ip route | awk '/default/ { print $3 }')

ping -q -c 1 $IP > /dev/null 2>&1
if [[ $? != 0 ]]; then
	sleep 10
	ping -q -c 1 $IP > /dev/null 2>&1
	if [[ $? != 0 ]]; then
		# Controleer of het modem wel bereikbaar is
		ping -q -c 1 192.168.178.1 > /dev/null 2>&1
		if [[ $? = 0 ]]; then
			csrftokenlogin="$(curl http://192.168.178.1/ -s | grep CSRFValue | sed 's/[^0-9]*//g')"
			curl --data "CSRFValue=${csrftokenlogin}&loginUsername=ziggo&loginPassword=draadloos" http://192.168.178.1/goform/Zlogin -s
			csrftokenreset="$(curl http://192.168.178.1/BridgeHome.asp -s | grep CSRFValue | sed 's/[^0-9]*//g')"
			curl --data "CSRFValue=${csrftokenreset}&resetBridgeHome=1" http://192.168.178.1/goform/BridgeHome -s
			echo "\nModem is gereset (hoop ik)"
		else
			echo "\nGeen internet en geen verbinding met het modem (of modem is doods)"
		fi
	fi
fi
